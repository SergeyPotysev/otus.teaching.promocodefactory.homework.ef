﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId = Roles.Single(x => x.Name == "Admin").Id,
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                RoleId = Roles.Single(x => x.Name == "PartnerManager").Id,
                AppliedPromocodesCount = 10
            },
        };

        
        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };
        
        
        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };

        
        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = Guid.Parse("EE698B7B-7999-448B-A73E-B6806D83DCAF"),
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        Preferences = new List<CustomerPreference>()
                        {
                            new CustomerPreference()
                            {
                                PreferenceId = Preferences.Single(x => x.Name == "Театр").Id
                            }
                        },
                        PromoCodes = new List<CustomerPromoCode>()
                        {
                            new CustomerPromoCode()
                            {
                                PromoCodeId = PromoCodes.Single(x => x.PartnerName == "Озон").Id
                            },
                            new CustomerPromoCode()
                            {
                                PromoCodeId = PromoCodes.Single(x => x.PartnerName == "Эльдорадо").Id
                            }
                        }
                    },
                    new Customer()
                    {
                        Id = Guid.Parse("B3D1D11A-499E-4BB3-BD14-05695341B974"),
                        Email = "yurik@mail.ru",
                        FirstName = "Юрий",
                        LastName = "Бояркин",
                        Preferences = new List<CustomerPreference>()
                        {
                            new CustomerPreference()
                            {
                                PreferenceId = Preferences.Single(x => x.Name == "Семья").Id
                            },
                            new CustomerPreference()
                            {
                                PreferenceId = Preferences.Single(x => x.Name == "Дети").Id
                            }
                        },
                        PromoCodes = new List<CustomerPromoCode>()
                        {
                            new CustomerPromoCode()
                            {
                                PromoCodeId = PromoCodes.Single(x => x.PartnerName == "Озон").Id
                            },
                            new CustomerPromoCode()
                            {
                                PromoCodeId = PromoCodes.Single(x => x.PartnerName == "Цирк Чудес").Id
                            }
                        }
                    }
                };

                return customers;
            }
        }


        public static IEnumerable<PromoCode> PromoCodes => new List<PromoCode>()
        {
            new PromoCode()
            {
                Id = Guid.Parse("C0366AC8-D6E1-472A-B34D-DD1D79195073"),
                Code = "A241",
                ServiceInfo = "Кэшбэк 5% при оплате покупки картой МИР!",
                BeginDate = new DateTime(2021, 1, 1),
                EndDate = new DateTime(2021, 12, 31),
                PartnerName = "Озон",
                PartnerManagerId = Roles.Single(x => x.Name == "PartnerManager").Id,
                PreferenceId = Preferences.Single(x => x.Name == "Театр").Id
            },
            new PromoCode()
            {
                Id = Guid.Parse("AFC0781D-D995-46AE-A1CE-D74BAAAA9633"),
                Code = "Z300",
                ServiceInfo = "Кодовое слово на 300 бонусов!",
                BeginDate = new DateTime(2021, 2, 1),
                EndDate = new DateTime(2021, 11, 15),
                PartnerName = "Эльдорадо",
                PartnerManagerId = Roles.Single(x => x.Name == "Admin").Id,
                PreferenceId = Preferences.Single(x => x.Name == "Семья").Id              
            },
            new PromoCode()
            {
                Id = Guid.Parse("A53B7349-DAEC-49E0-9B8D-C1DE54F3932F"),
                Code = "F825",
                ServiceInfo = "Скидка 10% на шоу-программы!",
                BeginDate = new DateTime(2021, 3, 1),
                EndDate = new DateTime(2022, 6, 30),
                PartnerName = "Цирк Чудес",
                PartnerManagerId = Roles.Single(x => x.Name == "Admin").Id,
                PreferenceId = Preferences.Single(x => x.Name == "Дети").Id              
            }
        };
    }
}
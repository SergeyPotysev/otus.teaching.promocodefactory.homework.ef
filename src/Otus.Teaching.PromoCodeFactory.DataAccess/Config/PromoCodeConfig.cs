﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Config
{
    public class PromoCodeConfig : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Code)
                .IsRequired();

            builder.Property(x => x.ServiceInfo)
                .IsRequired();

            builder.Property(x => x.BeginDate)
                .IsRequired();            
            
            builder.Property(x => x.EndDate)
                .IsRequired();        

            builder.Property(x => x.PartnerName)
                .IsRequired();     
        }
    }
}
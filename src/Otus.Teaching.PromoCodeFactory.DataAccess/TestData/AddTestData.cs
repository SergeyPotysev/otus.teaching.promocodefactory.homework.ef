﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.TestData;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Ef;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.TestData
{
    public class InitDatabase : IAddTestData
    {
        private readonly DataContext _dataContext;

        public InitDatabase(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void AddTestData()
        {
            // База данных удаляется, если она существует.
            _dataContext.Database.EnsureDeleted();
            // База данных создается, если она не существует.
            _dataContext.Database.EnsureCreated();
            
            _dataContext.AddRange(FakeDataFactory.Roles);
            _dataContext.AddRange(FakeDataFactory.Employees);
            _dataContext.AddRange(FakeDataFactory.Preferences);
            _dataContext.AddRange(FakeDataFactory.PromoCodes);
            
            _dataContext.AddRange(FakeDataFactory.Customers);
            
            _dataContext.SaveChanges();
            
            // Если вы ориентируетесь на реляционную базу данных и используете миграции, вы можете использовать метод
            // DbContext.Database.Migrate (), чтобы убедиться, что база данных создана и все миграции применены.
            // _dataContext.Database.Migrate();
        }
    }
}
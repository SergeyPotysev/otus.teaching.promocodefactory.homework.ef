﻿namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.TestData
{
    public interface IAddTestData
    {
        public void AddTestData();
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services
{
    public interface IPreferenceService
    {
        Task<IEnumerable<PreferenceResponse>> GetPreferenceCustomersAsync();
    }
}
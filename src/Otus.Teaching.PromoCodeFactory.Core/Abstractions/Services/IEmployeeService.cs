﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services
{
    public interface IEmployeeService
    {
        Task<Guid> AddEmployeeAsync(CreateOrEditEmployeeRequest employeeModel);

        Task<IEnumerable<EmployeeShortResponse>> GetEmployeesAsync();

        Task<EmployeeResponse> GetEmployeeByIdAsync(Guid id);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services
{
    public interface ICustomerService
    {
        Task<IEnumerable<CustomerShortResponse>> GetCustomersAsync();
        
        Task<Guid> AddCustomerAsync(CreateOrEditCustomerRequest customerModel);
        
        Task<Guid> DeleteCustomerAsync(Guid id);

        Task<CustomerPreferenceResponse> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request);
        
        Task<CustomerPromoCodeResponse> GetCustomerPromoCodesByIdAsync(Guid id);
        
        Task<CustomerPreferenceResponse> GetCustomerPreferencesByIdAsync(Guid id);
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer : BaseEntity
    {
        [MaxLength(50)]
        public string FirstName { get; set; }
        
        [MaxLength(50)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(30)]
        public string Email { get; set; }
        
        public virtual ICollection<CustomerPromoCode> PromoCodes { get; set; }

        public virtual ICollection<CustomerPreference> Preferences { get; set; }

        public Customer()
        {
            PromoCodes = new List<CustomerPromoCode>();
            Preferences = new List<CustomerPreference>();
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Models
{
    public class PromoCodeResponce
    {
        public Guid Id { get; set; }
        
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public string BeginDate { get; set; }

        public string EndDate { get; set; }
        
        public string Preference { get; set; }

        public string PartnerName { get; set; }
        
        public List<CustomerShortResponse> Customers { get; set; }
    }
}
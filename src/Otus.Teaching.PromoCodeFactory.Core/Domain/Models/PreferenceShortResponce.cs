﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Models
{
    public class PreferenceShortResponse
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}
﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Models
{
    public class CustomerPreference
    {
        public int Id { get; set; } 
        
        public Guid CustomerId { get; set; }
        
        public Guid PreferenceId { get; set; }
    }
}
﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Models
{
    public class CustomerPromoCode
    {
        public int Id { get; set; } 
        
        public Guid CustomerId { get; set; }
        
        public Guid PromoCodeId { get; set; }
    }
}
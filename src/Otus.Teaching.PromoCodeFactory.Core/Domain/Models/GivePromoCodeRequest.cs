﻿namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Models
{
    public class GivePromoCodeRequest
    {
        /// <summary>
        /// Дополнительная информация.
        /// </summary>
        public string ServiceInfo { get; set; }

        /// <summary>
        /// Нименование организации, выпустившей промоакцию: "Цирк Чудес", "Озон".
        /// </summary>
        public string PartnerName { get; set; }

        /// <summary>
        /// Текстовый код промокода: "L097", "W812".
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Текстовое наименование предпочтения: "Театр", "Семья", "Дети".
        /// </summary>
        public string Preference { get; set; }
        
        /// <summary>
        /// Дата начала действия промокода.
        /// </summary>
        public string BeginDate { get; set; }

        /// <summary>
        /// Дата завершения действия промокода.
        /// </summary>
        public string EndDate { get; set; }
    }
}
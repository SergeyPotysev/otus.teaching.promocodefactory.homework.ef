﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Mappers
{
    public class AutoMapperPreferenceProfiles : Profile
    {
        public AutoMapperPreferenceProfiles()
        {
            CreateMap<Preference, PreferenceResponse>();
            
            CreateMap<Preference, PreferenceShortResponse>();
        }
    }
}
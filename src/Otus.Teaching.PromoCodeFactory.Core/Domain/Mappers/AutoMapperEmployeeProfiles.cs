﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Mappers
{
    public class AutoMapperEmployeeProfiles : Profile
    {
        public AutoMapperEmployeeProfiles()
        {
            CreateMap<Employee, CreateOrEditEmployeeRequest>().ReverseMap();
            
            CreateMap<Employee, EmployeeShortResponse>();
            
            CreateMap<Employee, EmployeeResponse>();
        }
    }
}
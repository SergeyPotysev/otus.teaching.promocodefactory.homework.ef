﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Mappers
{
    public class AutoMapperRoleProfiles : Profile
    {
        public AutoMapperRoleProfiles()
        {
            CreateMap<Role, RoleItemResponse>();
        }
    }
}
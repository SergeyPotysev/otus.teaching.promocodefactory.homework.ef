using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Mappers
{
    public class AutoMapperCustomerProfiles : Profile
    {
        public AutoMapperCustomerProfiles()
        {
            CreateMap<Customer, CustomerShortResponse>();

            CreateMap<Customer, CustomerPromoCodeResponse>()
                .ForMember(d => d.PromoCodes, 
                    o => o.Ignore());
                
            CreateMap<Customer, CustomerPreferenceResponse>()
                .ForMember(d => d.Preferences, 
                    o => o.Ignore());
        }
    }
}
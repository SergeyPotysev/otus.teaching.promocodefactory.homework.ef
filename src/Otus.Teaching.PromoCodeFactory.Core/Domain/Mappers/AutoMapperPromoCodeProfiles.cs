﻿using System;
using System.Globalization;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Mappers
{
    public class AutoMapperPromoCodeProfiles : Profile
    {
        public AutoMapperPromoCodeProfiles()
        {
            CreateMap<GivePromoCodeRequest, PromoCode>()
                .ForMember(d => d.BeginDate, o => o.MapFrom(
                    s => DateTime.Parse(s.BeginDate)))
                .ForMember(d => d.EndDate, o => o.MapFrom(
                    s => DateTime.Parse(s.EndDate)))
                .ForMember(d => d.Id, o => o.MapFrom(
                    s => Guid.NewGuid()));

            CreateMap<GivePromoCodeRequest, PromoCodeResponce>();

            CreateMap<PromoCode, PromoCodeShortResponse>()
                .ForMember(d => d.BeginDate, o => o.MapFrom(
                    s => s.BeginDate.ToString(CultureInfo.CurrentCulture)))
                .ForMember(d => d.EndDate, o => o.MapFrom(
                s => s.EndDate.ToString(CultureInfo.CurrentCulture)));
        }
    }
}
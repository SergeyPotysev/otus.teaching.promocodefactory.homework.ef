﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IRepository<Employee> _employeeRepo;
        private readonly IMapper _mapper;

        public EmployeeService(IRepository<Employee> employeeRepo, IMapper mapper)
        {
            _employeeRepo = employeeRepo;
            _mapper = mapper;
        }

        
        public async Task<Guid> AddEmployeeAsync(CreateOrEditEmployeeRequest employeeModel)
        {
            var employee = _mapper.Map<Employee>(employeeModel);
            employee.Id = Guid.NewGuid();
            await _employeeRepo.AddAsync(employee);
            return employee.Id;
        }

        
        public async Task<IEnumerable<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepo.GetAllAsync();
            var employeesModelList = _mapper.Map<List<EmployeeShortResponse>>(employees);
            return employeesModelList;
        }

        
        public async Task<EmployeeResponse> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepo.GetByIdAsync(id);
            if (employee == null)
            {
                throw new Exception($"Employee with id:{id} not found.");
            }
 
            var employeeModel = _mapper.Map<EmployeeResponse>(employee);
            return employeeModel;
        }
    }
}

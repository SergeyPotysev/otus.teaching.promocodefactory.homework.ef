﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Ef;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class PromoCodeService : IPromoCodeService
    {
        private readonly IRepository<PromoCode> _promoCodeRepo;
        private readonly IMapper _mapper;
        private readonly DataContext _dataContext;
        private readonly IRepository<Customer> _customerRepo;

        public PromoCodeService(IRepository<PromoCode> promoCodeRepo, IMapper mapper, DataContext dataContext, 
            IRepository<Customer> customerRepo)
        {
            _promoCodeRepo = promoCodeRepo;
            _mapper = mapper;
            _dataContext = dataContext;
            _customerRepo = customerRepo;
        }
        
        
        public async Task<IEnumerable<PromoCodeShortResponse>> GetPromoCodesAsync()
        {
            var promoCodes = await _promoCodeRepo.GetAllAsync();
            var promoCodeShortResponse = _mapper.Map<List<PromoCodeShortResponse>>(promoCodes);
            return promoCodeShortResponse;
        }

        
        public async Task<PromoCodeResponce> AddPromoCodeAsync(GivePromoCodeRequest request)
        {
            var haveSuchPreference =  _dataContext.Preferences.Any(p => p.Name == request.Preference);
            if (!haveSuchPreference)
            {
                throw new Exception($"No such preference found: {request.Preference}");
            }
            
            var promoCode = _mapper.Map<PromoCode>(request);
            // Сотрудника назначить рандомно.
            promoCode.PartnerManagerId = SetRandomPartner();
            // По текстовому названию предпочтения получить его GUID.
            promoCode.PreferenceId = _dataContext.Preferences.Single(x => x.Name == request.Preference).Id;
            
            // Подготовить модель для ответа.
            var promoCodeResponce =  _mapper.Map<PromoCodeResponce>(request);
            promoCodeResponce.Id = promoCode.Id;
            promoCodeResponce.Customers = new List<CustomerShortResponse>();
            
            // Добавить промокод в БД.
            await _promoCodeRepo.AddAsync(promoCode);
            
            // Получить список клиентов, у которых есть такое предпочтение.
            var customers = _dataContext.Customers.Where(x => x.Preferences.Any(
                o => o.PreferenceId == promoCode.PreferenceId)).ToList();
            
            for (var i = 0; i < customers.Count; i++)
            {
                customers[i].PromoCodes.Add(
                    new CustomerPromoCode()
                    {
                        CustomerId = customers[i].Id,
                        // Добавить клиенту новый промокод.
                        PromoCodeId = promoCode.Id
                    });
                // Добавить в ответ клиента с выданным промокодом.
                promoCodeResponce.Customers.Add(_mapper.Map<CustomerShortResponse>(customers[i]));
                // Обновить клиента в БД.
                await _customerRepo.UpdateAsync(customers[i]);
            }
            
            return promoCodeResponce;
        }

        
        private Guid SetRandomPartner()
        {
            var partnerIds = _dataContext.Employees.Select(x => x.Id).ToArray();
            var idCount = partnerIds.Count();
            var i = new Random().Next(0, idCount);
            return partnerIds[i];
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Ef;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IRepository<Customer> _customerRepo;
        private readonly IMapper _mapper;
        private readonly DataContext _dataContext;

        public CustomerService(IRepository<Customer> customerRepo, IMapper mapper, DataContext dataContext)
        {
            _customerRepo = customerRepo;
            _mapper = mapper;
            _dataContext = dataContext;
        }


        public async Task<IEnumerable<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepo.GetAllAsync();
            var customersModelList = _mapper.Map<List<CustomerShortResponse>>(customers);
            return customersModelList;
        }


        public async Task<Guid> AddCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customerId = Guid.NewGuid();
            var customer = new Customer()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Id = customerId,
                PromoCodes = new List<CustomerPromoCode>(),
                Preferences = new List<CustomerPreference>()
            };
            
            foreach (var preferenceId in request.PreferenceIds)
            {
                customer.Preferences.Add(new CustomerPreference()
                {
                    CustomerId = customerId,
                    PreferenceId = preferenceId
                });
            }

            await _customerRepo.AddAsync(customer);
            return customerId;
        }

        
        public async Task<Guid> DeleteCustomerAsync(Guid id)
        {
            var customer = await _customerRepo.GetByIdAsync(id);
            if (customer == null)
            {
                throw new Exception($"Customer with id:{id} not found.");
            }
            
            customer.PromoCodes.Clear();
            
            await _customerRepo.DeleteAsync(customer);
            return id;
        }


        public async Task<CustomerPreferenceResponse> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepo.GetByIdAsync(id);
            if (customer == null)
            {
                throw new Exception($"Customer with id:{id} not found.");
            }

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.Preferences.Clear();
            foreach (var preferenceGuid in request.PreferenceIds)
            {
                var customerPreference = new CustomerPreference()
                {
                    CustomerId = id,
                    PreferenceId = preferenceGuid
                };
                customer.Preferences.Add(customerPreference);
            }

            await _customerRepo.UpdateAsync(customer);
            
            var customerPreferenceModel = _mapper.Map<CustomerPreferenceResponse>(customer);
            customerPreferenceModel.Preferences = new List<PreferenceShortResponse>();
            foreach (var customerPreference in customer.Preferences)
            {
                var preference = _dataContext.Preferences.Single(x => x.Id == customerPreference.PreferenceId);
                var preferenceShortResponse = _mapper.Map<PreferenceShortResponse>(preference);
                customerPreferenceModel.Preferences.Add(preferenceShortResponse);
            }
            
            return customerPreferenceModel;
        }


        public async Task<CustomerPromoCodeResponse> GetCustomerPromoCodesByIdAsync(Guid id)
        {
            var customer = await _customerRepo.GetByIdAsync(id);
            if (customer == null)
            {
                throw new Exception($"Customer with id:{id} not found.");
            }
 
            var customerModel = _mapper.Map<CustomerPromoCodeResponse>(customer);
            customerModel.PromoCodes = new List<PromoCodeShortResponse>();
            foreach (var customerPromoCode in customer.PromoCodes)
            {
                var promoCode = _dataContext.PromoCodes.Single(x => x.Id == customerPromoCode.PromoCodeId);
                var promoCodeShortResponse = _mapper.Map<PromoCodeShortResponse>(promoCode);
                customerModel.PromoCodes.Add(promoCodeShortResponse);
            }

            return customerModel;
        }
        
        
        public async Task<CustomerPreferenceResponse> GetCustomerPreferencesByIdAsync(Guid id)
        {
            var customer = await _customerRepo.GetByIdAsync(id);
            if (customer == null)
            {
                throw new Exception($"Customer with id:{id} not found.");
            }
 
            var customerPreferenceModel = _mapper.Map<CustomerPreferenceResponse>(customer);
            customerPreferenceModel.Preferences = new List<PreferenceShortResponse>();
            foreach (var customerPreference in customer.Preferences)
            {
                var preference = _dataContext.Preferences.Single(x => x.Id == customerPreference.PreferenceId);
                var preferenceShortResponse = _mapper.Map<PreferenceShortResponse>(preference);
                customerPreferenceModel.Preferences.Add(preferenceShortResponse);
            }

            return customerPreferenceModel;
        }
        
    }
}
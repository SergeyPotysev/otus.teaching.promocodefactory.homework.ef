﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class PreferenceService : IPreferenceService
    {
        private readonly IRepository<Preference> _preferenceRepo;
        private readonly IRepository<Customer> _customerRepo;
        private readonly IMapper _mapper;

        public PreferenceService(IRepository<Preference> preferenceRepo, IRepository<Customer> customerRepo, IMapper mapper)
        {
            _preferenceRepo = preferenceRepo;
            _customerRepo = customerRepo;
            _mapper = mapper;
        }
        
        
        public async Task<IEnumerable<PreferenceResponse>> GetPreferenceCustomersAsync()
        {
            var preferencesList = (await _preferenceRepo.GetAllAsync()).ToList();
            var customersList = (await _customerRepo.GetAllAsync()).ToList();
            var preferencesResponce = _mapper.Map<List<PreferenceResponse>>(preferencesList);
            
            for(var i = 0; i < preferencesList.Count(); i++)
            {
                preferencesResponce[i].Customers = new List<CustomerShortResponse>();
                foreach (var customer in customersList)
                {
                    var isCustomerPreference = customer.Preferences.Any(x => x.PreferenceId == preferencesList[i].Id);
                    if (!isCustomerPreference) continue;
                    var customerShortResponse = _mapper.Map<CustomerShortResponse>(customer);
                    preferencesResponce[i].Customers.Add(customerShortResponse);
                }
                // Рефакторинг Решарпера:
                // foreach (var customerShortResponse in from customer in customersList 
                //     let isCustomerPreference = customer.Preferences.Any(x => 
                //         x.PreferenceId == preferencesList[i].Id) where isCustomerPreference 
                //     select _mapper.Map<CustomerShortResponse>(customer))
                // {
                //     preferencesResponce[i].Customers.Add(customerShortResponse);
                // }
            }
            
            return preferencesResponce;
        }

    }
}
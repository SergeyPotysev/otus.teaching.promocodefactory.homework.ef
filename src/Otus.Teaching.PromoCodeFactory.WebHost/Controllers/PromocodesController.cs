﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды.
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase
    {
        private readonly IPromoCodeService _promoCodeService;

        public PromocodesController(IPromoCodeService promoCodeService)
        {
            _promoCodeService = promoCodeService;
        }
        
        
        /// <summary>
        /// Получить все промокоды.
        /// </summary>
        /// <returns>Список промокодов.</returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromoCodesAsync()
        {
            var promoCodeList = await _promoCodeService.GetPromoCodesAsync();
            return Ok(promoCodeList);
        }
        
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением.
        /// </summary>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task<ActionResult<PromoCodeResponce>> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            try
            {
                var newPromoCode = await _promoCodeService.AddPromoCodeAsync(request);
                return Ok(newPromoCode);
            }
            catch(Exception ex)
            {
                return BadRequest($"Failed to create a new promoCode: {ex.Message}");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты.
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerService _customerService;

        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }
        
        /// <summary>
        /// Получение списка клиентов.
        /// </summary>
        /// <returns>Список клиентов.</returns>
        [HttpGet]
        public async Task<IEnumerable<CustomerShortResponse>> GetCustomersAsync()
        {
            var customersModelList = await _customerService.GetCustomersAsync();
            return customersModelList;
        }
        
        
        /// <summary>
        /// Получение одного клиента вместе с выданными ему промокодами.
        /// </summary>
        /// <param name="id">GUID клиента.</param>
        /// <returns>Данные клиента с его промодами.</returns>
        [HttpGet("promocode/{id:guid}")]
        public async Task<ActionResult<CustomerPromoCodeResponse>> GetCustomerPromoCodesByIdAsync(Guid id)
        {
            var customerModel = await _customerService.GetCustomerPromoCodesByIdAsync(id);
            return customerModel;
        }
        
        
        /// <summary>
        /// Получение одного клиента вместе с его предпочтениями.
        /// </summary>
        /// <param name="id">GUID клиента.</param>
        /// <returns>Данные клиента с его предпочтениями.</returns>
        [HttpGet("preference/{id:guid}")]
        public async Task<ActionResult<CustomerPreferenceResponse>> GetCustomerPreferencesByIdAsync(Guid id)
        {
            var customerModel = await _customerService.GetCustomerPreferencesByIdAsync(id);
            return customerModel;
        }
        
        
        /// <summary>
        /// Создание нового клиента вместе с его предпочтениями.
        /// </summary>
        /// <param name="request">Модель данных клиента.</param>
        /// <returns>GUID нового клиента.</returns>
        [HttpPost("add")]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            try
            {
                var newCustomerId = await _customerService.AddCustomerAsync(request);
                return Ok(newCustomerId.ToString());
            }
            catch(Exception ex)
            {
                return BadRequest($"Failed to create a new customer: {ex.Message}");
            }
        }
        
        
        /// <summary>
        /// Изменение данных клиента вместе с его предпочтениями.
        /// </summary>
        /// <param name="id">GUID клиента.</param>
        /// <param name="request">Модель данных клиента.</param>
        /// <returns>Измененная модель клиента.</returns>
        [HttpPut("update/{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            try
            {
                var updatedCustomer = await _customerService.EditCustomersAsync(id, request);
                return Ok(updatedCustomer);
            }
            catch(Exception ex)
            {
                return BadRequest($"Failed to edit the customer: {ex.Message}");
            }
        }
        
        
        /// <summary>
        /// Удаление клиента вместе с выданными ему промокодами.
        /// </summary>
        /// <param name="id">GUID клиента.</param>
        /// <returns>GUID удаленного клиента.</returns>
        [HttpDelete("delete/{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            try
            {
                var deletedCustomerId = await _customerService.DeleteCustomerAsync(id);
                return Ok(deletedCustomerId.ToString());
            }
            catch(Exception ex)
            {
                return BadRequest($"Failed to delete the customer: {ex.Message}");
            }
        }
    }
}
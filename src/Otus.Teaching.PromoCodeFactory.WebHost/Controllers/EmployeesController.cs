﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники.
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;

        public EmployeesController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }
        
        
        /// <summary>
        /// Получить данные всех сотрудников.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employeesModelList = await _employeeService.GetEmployeesAsync();
            return employeesModelList;
        }

        
        /// <summary>
        /// Получить данные сотрудника по id.
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employeeModel = await _employeeService.GetEmployeeByIdAsync(id);
            return employeeModel;
        }
        
        
        /// <summary>
        /// Добавить нового сотрудника. 
        /// </summary>
        /// <param name="request">Модель данных сотрудника.</param>
        /// <returns>Статус-код выполнения операции.</returns>
        [HttpPost("add")]
        public async Task<IActionResult> AddEmployeeAsync(CreateOrEditEmployeeRequest request)
        {
            try
            {
                var responseId = await _employeeService.AddEmployeeAsync(request);
                return Ok(responseId.ToString());
            }
            catch(Exception ex)
            {
                return BadRequest($"Failed to create a new employee: {ex.Message}");
            }
        }

    }
}
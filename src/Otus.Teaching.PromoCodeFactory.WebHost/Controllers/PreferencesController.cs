﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
        /// <summary>
        /// Предпочтения.
        /// </summary>
        [ApiController]
        [Route("api/v1/[controller]")]
        public class PreferencesController : ControllerBase
        {
            private readonly IPreferenceService _preferenceService;
            private readonly ICustomerService _customerService;

            public PreferencesController(IPreferenceService preferenceService, ICustomerService customerService)
            {
                _preferenceService = preferenceService;
                _customerService = customerService;
            }
            
            /// <summary>
            /// Получение списка предпочтений c перечнем клиентов по каждому предпочтению.
            /// </summary>
            /// <returns>Список предпочтений.</returns>
            [HttpGet]
            public async Task<IEnumerable<PreferenceResponse>> GetPreferenceCustomersAsync()
            {
                var preferencesModelList = await _preferenceService.GetPreferenceCustomersAsync();
                return preferencesModelList;
            }
        }
}